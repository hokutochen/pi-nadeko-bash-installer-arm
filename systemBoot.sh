#!/bin/bash -e
root=$(pwd)
echo ""

cd ~/nadekobot
echo "Creating the service..."

echo "[Unit]
Description=NadekoBot service
After=network.target
StartLimitIntervalSec=60
StartLimitBurst=2

[Service]
Type=simple
User=$USER
WorkingDirectory=$PWD/output
# If you want Nadeko to be compiled prior to every startup, uncomment the lines
# below. Note  that it's not neccessary unless you are personally modifying the
# source code.
#ExecStartPre=/usr/share/dotnet-arm64/dotnet build ../src/NadekoBot/NadekoBot.csproj -c Release -o output/
ExecStart=/usr/share/dotnet-arm64/dotnet NadekoBot.dll
Restart=on-failure
RestartSec=5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=NadekoBot

[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/nadeko.service
sudo systemctl daemon-reload
sudo systemctl enable nadeko.service
sudo systemctl start nadeko.service

exit 0
